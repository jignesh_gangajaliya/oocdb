﻿using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus.Topics
{
    public class TopicClientWrapper : IServiceBusClientWrapper
    {
        #region Member Variables

        private readonly string _connectionString;
        private readonly string _topicName;
        private readonly TopicClient _topicClient;
        private SubscriptionClient _subscriptionClient;

        #endregion Member Variables

        #region Properties

        public string SubscriptionName { get; set; }

        #endregion Properties

        #region Constructors

        public TopicClientWrapper(string connectionString, string topicName)
        {
            _connectionString = connectionString;
            _topicName = topicName;
            _topicClient = TopicClient.CreateFromConnectionString(connectionString, topicName);
        }

        #endregion Constructors

        #region Event Handlers

        public void OnMessageAsync(Func<BrokeredMessage, Task> onMessageCallback, OnMessageOptions onMessageOptions)
        {
           
            _subscriptionClient = SubscriptionClient.CreateFromConnectionString(_connectionString, _topicName, SubscriptionName);
            
            _subscriptionClient.OnMessageAsync(onMessageCallback, onMessageOptions);
            
        }

        #endregion Event Handlers

        #region Methods

        public Task SendAsync(BrokeredMessage message)
        {
            return _topicClient.SendAsync(message);
            
        }

        public void Close()
        {
            if (_subscriptionClient != null)
            {
                _subscriptionClient.Close();
            }

            _topicClient.Close();
        }

        #endregion Methods
    }
}
