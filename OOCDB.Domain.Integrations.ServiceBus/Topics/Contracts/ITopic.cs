﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus.Topics.Contracts
{
    public interface ITopic<TTopic> : IDisposable
    {
        #region Properties

        string TopicName { get; }

        #endregion Properties

        #region Methods

        void DeleteTopic();
        
        Task Publish<TMessage>(TMessage message) where TMessage : class, new();

        Task Publish<TMessage>(TMessage message, Action<AggregateException> onError) where TMessage : class, new();

        Task Publish<TMessage>(BrokeredMessage message, Action<AggregateException> onError) where TMessage : class, new();

        Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError, OnMessageOptions onMessageOptions, CancellationTokenSource cancellationTokenSource);

        Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError, CancellationTokenSource cancellationTokenSource);

        Task Subscribe(Func<dynamic, Task> onMessage, CancellationTokenSource cancellationTokenSource);

        Task Subscribe(Func<dynamic, Task> onMessage, OnMessageOptions onMessageOptions);

        Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError);

        Task Subscribe(Func<dynamic, Task> onMessage);

        #endregion Methods
    }
}