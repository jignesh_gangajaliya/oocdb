﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using OOCDB.Domain.Integrations.ServiceBus.Topics.Contracts;

namespace OOCDB.Domain.Integrations.ServiceBus.Topics
{
    public sealed class Topic<TTopic> : ITopic<TTopic>
    {
        #region Member Variables

        private readonly INamespaceManager _namespaceManager;
        private readonly bool _deleteSubscriptionOnDispose;
        private readonly ServiceBusCommon<TTopic> _serviceBusCommon;

        #endregion Member Variables

        #region Properties

        public string TopicName
        {
            get
            {
                return ServiceBusCommon<TTopic>.GetName();
            }
        }

        public string SubscriptionId
        {
            get;
            private set;             
        }

        #endregion Properties

        #region Constructors

        public Topic(string connectionString)
            : this(new NamespaceManager(connectionString),
            new TopicClientWrapper(connectionString, ServiceBusCommon<TTopic>.GetName()), null, false, null)
        { }

        public Topic(string connectionString, string subscriptionId)
            : this(new NamespaceManager(connectionString),
            new TopicClientWrapper(connectionString, ServiceBusCommon<TTopic>.GetName()), subscriptionId, true, () => CreateSubscriberClient(connectionString, subscriptionId))
        { }

        public Topic(string connectionString, string subscriptionId, bool deleteSubscriptionOnDispose)
            : this(new NamespaceManager(connectionString),
            new TopicClientWrapper(connectionString, ServiceBusCommon<TTopic>.GetName()), subscriptionId, deleteSubscriptionOnDispose, () => CreateSubscriberClient(connectionString, subscriptionId))
        { }

        internal Topic(INamespaceManager namespaceManager, IServiceBusClientWrapper serviceBusClient, string subscriptionId, bool deleteSubscriptionOnDispose, Func<IServiceBusClientWrapper> subscriberClient) //Unit test seam 
        {
            SubscriptionId = subscriptionId;

            _namespaceManager = namespaceManager;
            _deleteSubscriptionOnDispose = deleteSubscriptionOnDispose;

            if (!_namespaceManager.TopicExists(ServiceBusCommon<TTopic>.GetName()))
            {
                _namespaceManager.CreateTopic(ServiceBusCommon<TTopic>.GetName());
            }

            _serviceBusCommon = new ServiceBusCommon<TTopic>(serviceBusClient, subscriberClient);
        }

        #endregion Constructors

        #region Methods

        public void DeleteTopic()
        {
            _namespaceManager.DeleteTopic(ServiceBusCommon<TTopic>.GetName());
        }

        public Task Subscribe(Func<dynamic, Task> onMessage)
        {
            return Subscribe(onMessage, null, null, new CancellationTokenSource());
        }

        public Task Subscribe(Func<dynamic, Task> onMessage, OnMessageOptions onMessageOptions)
        {
            return Subscribe(onMessage, null, onMessageOptions, new CancellationTokenSource());
        }

        public Task Subscribe(Func<dynamic, Task> onMessage, CancellationTokenSource cancellationTokenSource)
        {
            return Subscribe(onMessage, null, null, cancellationTokenSource);
        }

        public Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError)
        {
            return Subscribe(onMessage, onError, null, new CancellationTokenSource());
        }

        public Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError, CancellationTokenSource cancellationTokenSource)
        {
            return Subscribe(onMessage, onError, null, cancellationTokenSource);
        }

        public Task Subscribe(Func<dynamic, Task> onMessage, Action<Exception> onError, OnMessageOptions onMessageOptions, CancellationTokenSource cancellationTokenSource)
        {
            if (string.IsNullOrWhiteSpace(SubscriptionId))
            {
                throw new ArgumentNullException("subscriptionId", "Please supply a subscriptionId to the constructor for your subscription");
            }

            if (SubscriptionId.Length > 50)
            {
                throw new ArgumentException("subscriptionId provided to the constructor can't be > 50 characters", "subscriptionId");
            }

            if (!_namespaceManager.SubscriptionExists(ServiceBusCommon<TTopic>.GetName(), SubscriptionId))
            {
                _namespaceManager.CreateSubscription(ServiceBusCommon<TTopic>.GetName(), SubscriptionId);
            }
                
            return _serviceBusCommon.OnMessage(onMessage, onError, onMessageOptions, cancellationTokenSource);            
        }

        public Task Publish<TMessage>(TMessage message) where TMessage : class, new()
        {
            return Publish(message, null);
        }

        public Task Publish<TMessage>(TMessage message, Action<AggregateException> onError) where TMessage : class, new()
        {
            return _serviceBusCommon.Send(message, onError);
        }

        public Task Publish<TMessage>(BrokeredMessage brokeredMessage) where TMessage : class, new()
        {
            return Publish<TMessage>(brokeredMessage, null);
        }

        public Task Publish<TMessage>(BrokeredMessage brokeredMessage, Action<AggregateException> onError) where TMessage : class, new()
        {
            return _serviceBusCommon.Send<TMessage>(brokeredMessage, onError);
        }

        private static IServiceBusClientWrapper CreateSubscriberClient(string connectionString, string subscriptionId)
        {
            return new TopicClientWrapper(connectionString, ServiceBusCommon<TTopic>.GetName())
            {
                SubscriptionName = subscriptionId
            };
        }

        public void Dispose()
        {
            if (_deleteSubscriptionOnDispose && SubscriptionId != null)
            {
                _namespaceManager.DeleteSubscription(ServiceBusCommon<TTopic>.GetName(), SubscriptionId);
            }

            _serviceBusCommon.Dispose();
        }

        #endregion Methods
    }
}
