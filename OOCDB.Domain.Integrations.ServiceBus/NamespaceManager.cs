﻿using System;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    public sealed class NamespaceManager : INamespaceManager
    {
        #region Member Variables

        private readonly Microsoft.ServiceBus.NamespaceManager _namespaceManager;

        #endregion Member Variables

        #region Properties

        public string ConnectionString
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructors

        public NamespaceManager(string connectionString)
        {
            ConnectionString = connectionString;
            _namespaceManager = Microsoft.ServiceBus.NamespaceManager.CreateFromConnectionString(connectionString);
        }

        #endregion Constructors

        #region Methods

        public QueueDescription GetQueue(string path)
        {
            return _namespaceManager.GetQueue(path);
        }

        public TopicDescription GetTopic(string path)
        {
            return _namespaceManager.GetTopic(path);
        }

        public bool QueueExists(string path)
        {
            return _namespaceManager.QueueExists(path);
        }

        public bool TopicExists(string path)
        {
            return _namespaceManager.TopicExists(path);
        }

        public bool SubscriptionExists(string path, string subscriptionName)
        {
            return _namespaceManager.SubscriptionExists(path, subscriptionName);
        }

        public QueueDescription CreateQueue(string path, Settings settings)
        {
            var queueDescription = new QueueDescription(path);

            if (settings != null)
            {
                queueDescription.RequiresDuplicateDetection = settings.RequireDuplicateDetection;

                if (settings.MaxDeliveryCount.HasValue)
                    queueDescription.MaxDeliveryCount = settings.MaxDeliveryCount.Value;

                if (settings.DuplicateDetectionHistoryTimeWindow.HasValue)
                    queueDescription.DuplicateDetectionHistoryTimeWindow = settings.DuplicateDetectionHistoryTimeWindow.Value;
            }

            return _namespaceManager.CreateQueue(queueDescription);
        }

        public TopicDescription CreateTopic(string path)
        {
            return _namespaceManager.CreateTopic(new TopicDescription(path));
        }

        public SubscriptionDescription CreateSubscription(string path, string subscriptionName)
        {
            return _namespaceManager.CreateSubscription(path, subscriptionName);
        }
        public SubscriptionDescription CreateSubscriptionWithSession(string path, string subscriptionName)
        {
            SubscriptionDescription sub = new SubscriptionDescription(path, subscriptionName);
            sub.RequiresSession = true;
            return _namespaceManager.CreateSubscription(sub);

        }
        public void DeleteQueue(string path)
        {
            _namespaceManager.DeleteQueue(path);
        }

        public void DeleteTopic(string path)
        {
            _namespaceManager.DeleteTopic(path);
        }

        public void DeleteSubscription(string path, string subscriptionName)
        {
            _namespaceManager.DeleteSubscription(path, subscriptionName);
        }

        public bool SubscriptionExists(object serviceBus_Pub_Sub_SocialNetworkProfile_ServicePath, string subscriptionName)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}
