﻿using System;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    public sealed class Settings
    {
        #region Properties

        public int? MaxDeliveryCount { get; set; }

        public bool RequireDuplicateDetection { get; set; }

        public TimeSpan? DuplicateDetectionHistoryTimeWindow { get; set; }

        #endregion Properties

        #region Constructors

        public Settings()
        {
            MaxDeliveryCount = 10;
        }

        #endregion Constructors

        #region Methods

        internal void Vaildate<T>(INamespaceManager namespaceManager)
        {
            var queueDescription = namespaceManager.GetQueue(ServiceBusCommon<T>.GetName());
            if (
                (MaxDeliveryCount.HasValue && queueDescription.MaxDeliveryCount != MaxDeliveryCount) ||
                queueDescription.RequiresDuplicateDetection != RequireDuplicateDetection ||
                (DuplicateDetectionHistoryTimeWindow.HasValue &&
                 queueDescription.DuplicateDetectionHistoryTimeWindow != DuplicateDetectionHistoryTimeWindow)
                )
            {
                throw new InvalidOperationException(
                    "The Azure SDK 2.3 only lets you set settings when first creating the Queue. For existing queues you will need to change the settings manually via the Azure portal.");
            }
        }

        #endregion Methods
    }
}
