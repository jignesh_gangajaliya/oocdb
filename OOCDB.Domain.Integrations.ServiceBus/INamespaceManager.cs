﻿using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    public interface INamespaceManager
    {
        #region Properties

        string ConnectionString { get; }

        #endregion Properties

        #region Methods

        bool QueueExists(string path);

        bool TopicExists(string path);

        bool SubscriptionExists(string path, string subscriptionName);

        QueueDescription CreateQueue(string path, Settings settings);

        TopicDescription CreateTopic(string path);

        SubscriptionDescription CreateSubscription(string path, string subscriptionName);

        QueueDescription GetQueue(string path);

        TopicDescription GetTopic(string path);

        void DeleteQueue(string path);

        void DeleteTopic(string path);

        void DeleteSubscription(string path, string subscriptionName);

        #endregion Methods     
    }
}
