﻿using System;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus.Exceptions
{
    public class OnMessageException : Exception
    {
        #region Properties

        public BrokeredMessage BrokeredMessage { get; private set; }

        #endregion Properties

        #region Methods

        public OnMessageException(BrokeredMessage brokeredMessage, string message, Exception ex) : base(message, ex)
        {
            BrokeredMessage = brokeredMessage;
        }

        #endregion Methods
    }
}
