﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    internal static class BrokeredMessageExtensions
    {
        #region Methods

        public static object GetBody(this BrokeredMessage brokeredMessage)
        {
            if (string.IsNullOrWhiteSpace(brokeredMessage.ContentType))
            {
                throw new InvalidOperationException(
                    "ContentType must be set to the Type of the brokeredMessage. Please send the brokeredMessage using NPRO.Domain.Integrations.ServiceBus.Queues");
            }

            var serializer = new DataContractSerializer(Type.GetType(brokeredMessage.ContentType, true));

            using (var stream = brokeredMessage.GetBody<Stream>())
            {
                using (var binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
                {
                    return serializer.ReadObject(binaryReader);
                }
            }
        }

        #endregion Methods
    }
}
