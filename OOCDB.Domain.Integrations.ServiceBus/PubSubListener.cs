﻿using Microsoft.ServiceBus.Messaging;
using OOCDB.Domain.Integrations.ServiceBus.Queues;
using OOCDB.Domain.Integrations.ServiceBus.Topics;
using NPRO.Core.Utilities;


namespace OOCDB.Domain.Integrations.ServiceBus
{
    public sealed class PubSubListener
    {
        private TopicClientWrapper accountClient;
        private TopicClientWrapper accountCategoryClient;
        private TopicClientWrapper accountCategoryGroupClient;
        private TopicClientWrapper accountCategoryMapClient;
        private TopicClientWrapper accountUserClient;
        private TopicClientWrapper addOnOrderLinesClient;
        private TopicClientWrapper appActivityClient;
        private TopicClientWrapper beaconClient;
        private TopicClientWrapper campaignActivityClient;
        private TopicClientWrapper campaignActivityTypeClient;
        private TopicClientWrapper conversationClient;
        private TopicClientWrapper deviceClient;
        private TopicClientWrapper engagementClient;
        private TopicClientWrapper iBeaconProximityClient;
        private TopicClientWrapper interestClient;
        private TopicClientWrapper locationClient;
        private TopicClientWrapper modifierClient;
        private TopicClientWrapper modifierGroupClient;
        private TopicClientWrapper offerKronerDetailsClient;
        private TopicClientWrapper orderClient;
        private TopicClientWrapper orderLineClient;
        private TopicClientWrapper productClient;
        private TopicClientWrapper productCategoryClient;
        private TopicClientWrapper productVariantClient;
        private TopicClientWrapper proximityClient;
        private TopicClientWrapper requestClient;
        private TopicClientWrapper roleClient;
        private TopicClientWrapper rolePermissionClient;
        private TopicClientWrapper socialNetworkProfileClient;
        private TopicClientWrapper userClient;
        private TopicClientWrapper userRoleClient;
        private TopicClientWrapper userInterestClient;

        private NamespaceManager namespaceManagerPubSub;
        static readonly PubSubListener _instance = new PubSubListener();

        public static PubSubListener Instance
        {
            get
            {
                return _instance;
            }
        }

        PubSubListener()
        {
          
            namespaceManagerPubSub = new NamespaceManager(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Account_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Account_ServicePath);
            }
            accountClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Account_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategory_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategory_ServicePath);
            }
            accountCategoryClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategory_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryGroup_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryGroup_ServicePath);
            }
            accountCategoryGroupClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryGroup_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryMap_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryMap_ServicePath);
            }
            accountCategoryMapClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AccountCategoryMap_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AccountUsers_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AccountUsers_ServicePath);
            }
            accountUserClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AccountUsers_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AddOnOrderLines_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AddOnOrderLines_ServicePath);
            }
            addOnOrderLinesClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AddOnOrderLines_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_AppActivity_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_AppActivity_ServicePath);
            }
            appActivityClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_AppActivity_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Beacon_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Beacon_ServicePath);
            }
            beaconClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Beacon_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivity_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivity_ServicePath);
            }
            campaignActivityClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivity_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivityType_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivityType_ServicePath);
            }
            campaignActivityTypeClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_CampaignActivityType_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Conversation_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Conversation_ServicePath);
            }
            conversationClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Conversation_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Device_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Device_ServicePath);
            }
            deviceClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Device_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Engagement_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Engagement_ServicePath);
            }
            engagementClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Engagement_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_iBeaconProximity_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_iBeaconProximity_ServicePath);
            }
            iBeaconProximityClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_iBeaconProximity_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Interest_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Interest_ServicePath);
            }
            interestClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Interest_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Location_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Location_ServicePath);
            }
            locationClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Location_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Modifier_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Modifier_ServicePath);
            }
            modifierClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Modifier_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_ModifierGroup_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_ModifierGroup_ServicePath);
            }
            modifierGroupClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_ModifierGroup_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_OfferKronerDetails_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_OfferKronerDetails_ServicePath);
            }
            offerKronerDetailsClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_OfferKronerDetails_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Order_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Order_ServicePath);
            }
            orderClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Order_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_OrderLines_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_OrderLines_ServicePath);
            }
            orderLineClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_OrderLines_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Product_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Product_ServicePath);
            }
            productClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Product_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_ProductCategory_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_ProductCategory_ServicePath);
            }
            productCategoryClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_ProductCategory_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_ProductVariant_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_ProductVariant_ServicePath);
            }
            productVariantClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_ProductVariant_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Proximity_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Proximity_ServicePath);
            }
            proximityClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Proximity_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Request_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Request_ServicePath);
            }
            requestClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Request_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_Role_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_Role_ServicePath);
            }
            roleClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_Role_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_RolePermission_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_RolePermission_ServicePath);
            }
            rolePermissionClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_RolePermission_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_SocialNetworkProfile_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_SocialNetworkProfile_ServicePath);
            }
            socialNetworkProfileClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_SocialNetworkProfile_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_User_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_User_ServicePath);
            }
            userClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_User_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_UserRoles_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_UserRoles_ServicePath);
            }
            userRoleClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_UserRoles_ServicePath);

            if (!namespaceManagerPubSub.TopicExists(ConfigurationSettings.ServiceBus_Pub_Sub_UserInterests_ServicePath))
            {
                namespaceManagerPubSub.CreateTopic(ConfigurationSettings.ServiceBus_Pub_Sub_UserInterests_ServicePath);
            }
            userInterestClient = new TopicClientWrapper(ConfigurationSettings.ServiceBus_Pub_Sub_ConnectionString, ConfigurationSettings.ServiceBus_Pub_Sub_UserInterests_ServicePath);

        }

        public void HandlePubSubEvents(Core.Entities.Models.PubSubMessage message)
        {
            
            if(message==null)
            {
                return;
            }
            string name = message.Name;
            message.Name= name.Replace("NPRO.Core.Entities.", string.Empty).Replace("NPRO.Core.FastTrackOrder.Entities.",string.Empty).Replace("NPRO.Core.Concierge.Entities.",string.Empty).ToLower();
            BrokeredMessage brokerMessage = new BrokeredMessage(message);
            LargeMessageManager manager;
            switch (name)
            {

                case "NPRO.Core.Entities.Account":
                    manager = new LargeMessageManager(accountClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.AccountCategory":
                    manager = new LargeMessageManager(accountCategoryClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.AccountCategoryGroup":
                    manager = new LargeMessageManager(accountCategoryGroupClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.AccountCategoryMap":
                    manager = new LargeMessageManager(accountCategoryMapClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.AccountUsers":
                    manager = new LargeMessageManager(accountUserClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.AddOnOrderLines":
                    manager = new LargeMessageManager(addOnOrderLinesClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.AppActivity":
                    manager = new LargeMessageManager(appActivityClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Beacon":
                    manager = new LargeMessageManager(beaconClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.CampaignActivity":
                    manager = new LargeMessageManager(campaignActivityClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.CampaignActivityType":
                    manager = new LargeMessageManager(campaignActivityTypeClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Concierge.Entities.Conversation":
                    manager = new LargeMessageManager(conversationClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Device":
                    manager = new LargeMessageManager(deviceClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Engagement":
                    manager = new LargeMessageManager(engagementClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.iBeaconProximity":
                    manager = new LargeMessageManager(iBeaconProximityClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Interest":
                    manager = new LargeMessageManager(interestClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Location":
                    manager = new LargeMessageManager(locationClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.Modifier":
                    manager = new LargeMessageManager(modifierClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.ModifierGroup":
                    manager = new LargeMessageManager(modifierGroupClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.OfferKronerDetails":
                    manager = new LargeMessageManager(offerKronerDetailsClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.Order":
                    manager = new LargeMessageManager(orderClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.OrderLines":
                    manager = new LargeMessageManager(orderLineClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.Product":
                    manager = new LargeMessageManager(productClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.ProductCategory":
                    manager = new LargeMessageManager(productCategoryClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.FastTrackOrder.Entities.ProductVariant":
                    manager = new LargeMessageManager(productVariantClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Proximity":
                    manager = new LargeMessageManager(proximityClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Concierge.Entities.Request":
                    manager = new LargeMessageManager(requestClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.Role":
                    manager = new LargeMessageManager(roleClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.RolePermission":
                    manager = new LargeMessageManager(rolePermissionClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.SocialNetworkProfile":
                    manager = new LargeMessageManager(socialNetworkProfileClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.User":
                    manager = new LargeMessageManager(userClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.UserRoles":
                    manager = new LargeMessageManager(userRoleClient);
                    manager.Send(brokerMessage);
                    break;

                case "NPRO.Core.Entities.UserInterests":
                    manager = new LargeMessageManager(userInterestClient);
                    manager.Send(brokerMessage);
                    break;
                default:
                    break;
            }

        }

       
    }
}
