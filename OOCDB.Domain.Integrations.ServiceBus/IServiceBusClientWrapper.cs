﻿using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    internal interface IServiceBusClientWrapper
    {
        #region Methods

        void OnMessageAsync(Func<BrokeredMessage, Task> onMessageCallback, OnMessageOptions onMessageOptions);

        Task SendAsync(BrokeredMessage message);

        void Close();

        #endregion Methods
    }
}
