﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using OOCDB.Domain.Integrations.ServiceBus.Queues.Contracts;

namespace OOCDB.Domain.Integrations.ServiceBus.Queues
{
    public sealed class Queue<TQueue> : IQueue<TQueue>
    {
        #region Member Variables

        private readonly INamespaceManager _namespaceManager;
        private readonly ServiceBusCommon<TQueue> _serviceBusCommon;

        #endregion Member Variables

        #region Properties

        public string QueueName
        {
            get
            {
                return ServiceBusCommon<TQueue>.GetName();                 
            }
        }

        #endregion Properties

        #region Constructors

        public Queue(string connectionString)
            : this(
                new NamespaceManager(connectionString),
                new QueueClientWrapper(connectionString, ServiceBusCommon<TQueue>.GetName()),
                () => CreatePumpClient(connectionString), null)
        {
        }

        public Queue(string connectionString, Settings settings)
            : this(
                new NamespaceManager(connectionString),
                new QueueClientWrapper(connectionString, ServiceBusCommon<TQueue>.GetName()),
                () => CreatePumpClient(connectionString), settings)
        {
        }

        internal Queue(INamespaceManager namespaceManager, IServiceBusClientWrapper serviceBusClient,
            Func<IServiceBusClientWrapper> queuePumpClient, Settings settings)
        {
            _namespaceManager = namespaceManager;

            if (!_namespaceManager.QueueExists(ServiceBusCommon<TQueue>.GetName()))
            {
                _namespaceManager.CreateQueue(ServiceBusCommon<TQueue>.GetName(), settings ?? new Settings());
            }

            else if (settings != null)
            {
                settings.Vaildate<TQueue>(namespaceManager);
            }

            _serviceBusCommon = new ServiceBusCommon<TQueue>(serviceBusClient, queuePumpClient);
        }

        #endregion Constructors

        #region Methods

        public void DeleteQueue()
        {
            _namespaceManager.DeleteQueue(ServiceBusCommon<TQueue>.GetName());
        }

        public Task Pump(Func<dynamic, Task> onMessage)
        {
            return Pump(onMessage, null, null, new CancellationTokenSource());
        }

        public Task Pump(Func<dynamic, Task> onMessage, OnMessageOptions onMessageOptions)
        {
            return Pump(onMessage, null, onMessageOptions, new CancellationTokenSource());
        }

        public Task Pump(Func<dynamic, Task> onMessage, CancellationTokenSource cancellationTokenSource)
        {
            return Pump(onMessage, null, null, cancellationTokenSource);
        }

        public Task Pump(Func<dynamic, Task> onMessage, Action<Exception> onError)
        {
            return Pump(onMessage, onError, null, new CancellationTokenSource());
        }

        public Task Pump(Func<dynamic, Task> onMessage, Action<Exception> onError,
            CancellationTokenSource cancellationTokenSource)
        {
            return Pump(onMessage, onError, null, cancellationTokenSource);
        }

        public Task Pump(Func<dynamic, Task> onMessage, Action<Exception> onError, OnMessageOptions onMessageOptions,
            CancellationTokenSource cancellationTokenSource)
        {
            return _serviceBusCommon.OnMessage(onMessage, onError, onMessageOptions, cancellationTokenSource);
        }

        public Task Enqueue<TMessage>(TMessage message) where TMessage : class, new()
        {
            return Enqueue(message, null);
        }

        public Task Enqueue<TMessage>(TMessage message, Action<AggregateException> onError)
            where TMessage : class, new()
        {
            return _serviceBusCommon.Send(message, onError);
        }

        public Task Enqueue<TMessage>(BrokeredMessage brokeredMessage) where TMessage : class, new()
        {
            return Enqueue<TMessage>(brokeredMessage, null);
        }

        public Task Enqueue<TMessage>(BrokeredMessage brokeredMessage, Action<AggregateException> onError)
            where TMessage : class, new()
        {
            return _serviceBusCommon.Send<TMessage>(brokeredMessage, onError);
        }

        private static IServiceBusClientWrapper CreatePumpClient(string connectionString)
        {
            return new QueueClientWrapper(connectionString, ServiceBusCommon<TQueue>.GetName());
        }

        public void Dispose()
        {
            _serviceBusCommon.Dispose();
        }

        #endregion Methods
    }
}
