﻿using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace OOCDB.Domain.Integrations.ServiceBus.Queues
{
    public class QueueClientWrapper : IServiceBusClientWrapper
    {
        #region Member Variables

        private readonly QueueClient _queueClient;

        #endregion Member Variables

        #region Event Handlers

        public void OnMessageAsync(Func<BrokeredMessage, Task> onMessageCallback, OnMessageOptions onMessageOptions)
        {
            _queueClient.OnMessageAsync(onMessageCallback, onMessageOptions);
        }

        public void OnMessage(Action<BrokeredMessage> onMessageCallback, OnMessageOptions onMessageOptions)
        {
            _queueClient.OnMessage(onMessageCallback, onMessageOptions);
        }

        #endregion Event Handlers

        #region Methods

        public QueueClientWrapper(string connectionString, string queueName)
        {
            _queueClient = QueueClient.CreateFromConnectionString(connectionString, queueName);
        }        

        public Task SendAsync(BrokeredMessage message)
        {
            return _queueClient.SendAsync(message);
        }

        public void Close()
        {
            _queueClient.Close();
        }

        #endregion Methods
    }
}
