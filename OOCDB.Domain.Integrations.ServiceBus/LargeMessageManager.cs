﻿using Microsoft.ServiceBus.Messaging;
using OOCDB.Domain.Integrations.ServiceBus.Topics;
using OOCDB.Core.Entities.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOCDB.Domain.Integrations.ServiceBus
{
  
    public class LargeMessageManager
    {
        private static int SubMessageBodySize = 192 * 1024;
        private SubscriptionClient m_SubscriptionClient;
        private TopicClientWrapper m_TopicClient;
        public MessageSession _session;
        public SubscriptionClient storeSubscription { set { m_SubscriptionClient = value; } }
        public LargeMessageManager()  {}
        public LargeMessageManager(TopicClientWrapper topicClient)
        {
            m_TopicClient = topicClient;
        }
        public void Send(BrokeredMessage message)
        {
            long messageBodySize = message.Size;
            int nrSubMessages = (int)(messageBodySize / SubMessageBodySize);
            if (messageBodySize % SubMessageBodySize != 0)
            {
                nrSubMessages++;
            }
            string sessionId = Guid.NewGuid().ToString();
            Console.WriteLine("Message session Id: " + sessionId);
          
            Stream bodyStream = message.GetBody<Stream>();
            int messageSeq = 0;
            for (int streamOffest = 0; streamOffest < messageBodySize; streamOffest += SubMessageBodySize)
            {
                messageSeq++;
                long arraySize = (messageBodySize - streamOffest) > SubMessageBodySize ? SubMessageBodySize : messageBodySize - streamOffest;
                byte[] subMessageBytes = new byte[arraySize];
                int result = bodyStream.Read(subMessageBytes, 0, (int)arraySize);
                MemoryStream subMessageStream = new MemoryStream(subMessageBytes);
                BrokeredMessage subMessage = new BrokeredMessage(subMessageStream, true);
                subMessage.Label = messageSeq.ToString();
                subMessage.SessionId = sessionId;
                m_TopicClient.SendAsync(subMessage);
                Console.Write(".");
            }
            Console.WriteLine("Done!");
        }

        public PubSubMessage Receive(BrokeredMessage message)
        {
            MemoryStream largeMessageStream = new MemoryStream();
            MessageSession session = _session;
            Console.WriteLine("Message session Id: " + session.SessionId);
            Console.Write("Receiving sub messages");

            Dictionary<int, Stream> orderedDic = new Dictionary<int, Stream>();
            Stream subMessageStream = message.GetBody<Stream>();
            orderedDic.Add(int.Parse(message.Label), subMessageStream);
            message.Complete();
            while (true)
            {
                BrokeredMessage subMessage = session.Receive();

                if (subMessage != null)
                {
                    Stream subMessageStr = subMessage.GetBody<Stream>();
                    orderedDic.Add(int.Parse(subMessage.Label), subMessageStr);
                    subMessage.Complete();
                    //Console.Write(".");
                }
                else
                {
                    //Console.WriteLine("Done!");

                    for (int i = 1; i <= orderedDic.Count; i++)
                    {
                        Stream MessageStream;
                        orderedDic.TryGetValue(i, out MessageStream);
                        if (MessageStream != null)
                            MessageStream.CopyTo(largeMessageStream);
                    }
                    break;
                }
            }

            BrokeredMessage largeMessage = new BrokeredMessage(largeMessageStream, true);
            var pubSubMessage = largeMessage.GetBody<Core.Entities.Models.PubSubMessage>();
            return pubSubMessage;
        }
    }
}
