﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using OOCDB.Domain.Integrations.ServiceBus.Exceptions;

namespace OOCDB.Domain.Integrations.ServiceBus
{
    internal sealed class ServiceBusCommon<TQueue> : IDisposable
    {
        #region Member Variables

        private readonly IServiceBusClientWrapper _serviceBusClient;
        private readonly Func<IServiceBusClientWrapper> _onMessageClient;
        private CancellationTokenSource _cancellationTokenSource;

        #endregion Member Variables

        #region Constructors

        internal ServiceBusCommon(IServiceBusClientWrapper serviceBusClient, Func<IServiceBusClientWrapper> onMessageClient)
        {
            _serviceBusClient = serviceBusClient;
            _onMessageClient = onMessageClient;
        }

        #endregion Constructors

        #region Event Handlers

        public Task OnMessage(Func<dynamic, Task> onMessage, Action<Exception> onError, OnMessageOptions onMessageOptions, CancellationTokenSource cancellationTokenSource)
        {
            if (_cancellationTokenSource != null)
            {
                throw new InvalidOperationException("You can only start one pump at a time per instance");
            }

            if (onMessage == null)
            {
                throw new ArgumentNullException("onMessage",
                    "You must provide a Task to be invoked when the pump receives a brokeredMessage");
            }

            onMessageOptions = onMessageOptions ?? new OnMessageOptions();

            if (!onMessageOptions.AutoComplete)
            {
                throw new InvalidOperationException(
                    "This OnMessage cannot work with OnMessageOptions.AutoComplete set to false");
            }

            _cancellationTokenSource = cancellationTokenSource;

            var onMessageClient = _onMessageClient();

            return Task.Run(() =>
            {
                onMessageOptions.ExceptionReceived += (sender, args) => HandleOnMessageError(args.Exception, onError);

                onMessageClient.OnMessageAsync(async receivedMessage =>
                {
                    try
                    {
                        await onMessage(receivedMessage.Clone().GetBody());
                    }
                    catch (Exception ex)
                    {
                        throw new OnMessageException(receivedMessage.Clone(), ex.Message, ex);
                    }

                }, onMessageOptions);

                _cancellationTokenSource.Token.WaitHandle.WaitOne();
            }, cancellationTokenSource.Token)
            .ContinueWith(t =>
            {
                if (t.Exception != null)
                    HandleOnMessageError(t.Exception, onError);

                onMessageClient.Close();
            });
        }

        #endregion Event Handlers

        #region Methods
        
        public Task Send<TMessage>(TMessage message, Action<AggregateException> onError) where TMessage : class, new()
        {
            if (message == null)
            {
                throw new ArgumentNullException("message", "Message can't be null");
            }

            onError = onError ?? (exception => { throw exception; });

            return _serviceBusClient
                    .SendAsync(new BrokeredMessage(message) { ContentType = typeof(TMessage).AssemblyQualifiedName })
                    .ContinueWith(task => HandleEnqueueError(task.Exception, onError));
        }

        public Task Send<TMessage>(BrokeredMessage brokeredMessage, Action<AggregateException> onError) where TMessage : class, new()
        {
            if (brokeredMessage == null)
            {
                throw new ArgumentNullException("brokeredMessage", "Message can't be null");
            }

            if (brokeredMessage.ContentType != null)
            {
                throw new ArgumentException("producer setting the ContentType is not supported");
            }

            brokeredMessage.ContentType = typeof(TMessage).AssemblyQualifiedName;
            brokeredMessage.Clone().GetBody();

            return _serviceBusClient
                    .SendAsync(brokeredMessage)
                    .ContinueWith(task => HandleEnqueueError(task.Exception, onError));
        }

        internal static string GetName()
        {
            var queueName = typeof(TQueue).FullName;

            if (queueName.Length > 260)
            {
                throw new ArgumentException(
                    string.Format("Queue name can't be > 260 characters. Make your namespace or class name shorter."));
            }

            return queueName;
        }

        private static void HandleEnqueueError(AggregateException ex, Action<AggregateException> onError)
        {
            onError = onError ?? (exception => { throw exception; });

            if (ex == null)
            {
                return;
            }

            onError(ex);
        }

        private static void HandleOnMessageError(Exception ex, Action<Exception> onError)
        {
            onError = onError ?? (exception => { });

            if (ex == null)
            {
                return;
            }

            if (ex is OperationCanceledException)
            {
                return;
            }

            var aggregateException = ex as AggregateException;

            if (aggregateException != null)
            {
                if (aggregateException.InnerExceptions.Count == 1)
                {
                    onError(ex.InnerException);
                    return;
                }

                onError(aggregateException.Flatten());
                return;
            }

            onError(ex);
        }

        public void Dispose()
        {
            try
            {
                if (_cancellationTokenSource != null && !_cancellationTokenSource.IsCancellationRequested)
                    _cancellationTokenSource.Cancel();

                _serviceBusClient.Close();
            }
            catch
            {
                
            }
        }

        #endregion Methods
    }
}
