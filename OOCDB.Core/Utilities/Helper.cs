﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;


namespace OOCDB.Core.Utilities
{
    public static class Helper
    {
        public static string ConvertToUnicode(string data)
        {
            //return Encoding.ASCII.GetString(
            //        Encoding.Convert(
            //        Encoding.UTF8,
            //        Encoding.GetEncoding(
            //        Encoding.ASCII.EncodingName,
            //        new EncoderReplacementFallback(string.Empty),
            //        new DecoderExceptionFallback()),
            //        Encoding.UTF8.GetBytes(data)));
         
            return HttpUtility.HtmlDecode(data);
        }
        public static string ToHtmlInnerText(string htmlText)
        {
            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);
            string resultText = regex.Replace(htmlText, " ").Trim();
            return resultText;
        }
        public static DateTime GetDateTime(long milliSeconds)
        {
            return (new DateTime(1970, 1, 1)).AddMilliseconds(milliSeconds);
        }

        public static DateTime GetDateTimeFromPubNubStamp(long pubnubTime)
        {
            double tt = +pubnubTime / 10000;
            double miliseconds = Math.Ceiling(tt);
            return (new DateTime(1970, 1, 1)).AddMilliseconds(miliseconds);
        }

        public static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(double.Parse(unixTimeStamp));
            return dtDateTime;
        }

        public static int ToEpoch(this DateTime date)
        {
            if (date == null) return int.MinValue;
            DateTime epoch = new DateTime(1970, 1, 1);
            TimeSpan epochTimeSpan = date - epoch;
            return (int)epochTimeSpan.TotalSeconds;
        }
        public static DateTime ToDateTime(this string pEpoch)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            double EpochSeconds = 0;
            double.TryParse(pEpoch, out EpochSeconds);
            dtDateTime = dtDateTime.AddSeconds(EpochSeconds).ToLocalTime();
            return dtDateTime;
        }

        public static string ConvertToTimestamp(DateTime value)
        {
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            string Total = ((double)span.TotalSeconds).ToString();
            string CalculatedTime = (Total.Contains(".") ? Total.Remove(Total.IndexOf(".")) : Total);
            return CalculatedTime;
        }
        public static long GetMilliseconds(DateTime dateTime)
        {
            DateTime date = new DateTime(1970, 1, 1);
            TimeSpan remove = new TimeSpan(date.Ticks);
            TimeSpan timeStamp = new TimeSpan(dateTime.Ticks);
            return (long)(timeStamp.TotalMilliseconds) - (long)(remove.TotalMilliseconds);
        }

        public static string GenerateRandomPassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            var upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var num = "0123456789";
            var lower = "abcdefghijklmnopqrstuvwxyz";

        
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                if (i == 3)
                {
                    stringChars[i] = upper[random.Next(upper.Length)];
                }
                else if (i == 6)
                {
                    stringChars[i] = num[random.Next(num.Length)];
                }
                else if(i==7)
                {
                    stringChars[i] = lower[random.Next(lower.Length)];
                }
                else
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }
            }

           

            return new String(stringChars);


        }
    }
}
