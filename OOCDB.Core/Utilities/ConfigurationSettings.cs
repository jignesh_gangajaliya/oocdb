﻿using System;
using System.Configuration;

namespace OOCDB.Core.Utilities
{
    public class ConfigurationSettings
    {
        #region Member Variables

        public static string NPRODbConnection;
        public static bool IsSyncDB;
        public static bool IsSyncCRM;
        public static bool IsPubSubDB;
        public static string KeepDatabaseConnectionOpen;
        public static Uri CRM_OrganizationUri;
        public static string CRM_Username;
        public static string CRM_Password;
        public static string CRM_Campaign_Instagram;
        public static string CRM_Campaign_Pinterest;
        public static string CRM_Campaign_WordPress;
        public static string CRM_Campaign_Offer;
        public static string CRM_Campaign_Event;
        public static string CRM_Campaign_Article;
        public static string InstagramURL;
        public static string InstagramSearchURL;
        public static string InstagramHashtagURL;
        public static string InstagramUserURL;
        public static bool IsFirstTimeScheduler;
        public static string PinterestURL;
        public static string WordPressPostURL;
        public static string WordPressEventURL;
        public static string ServiceBus_CRM_ServicePath;
        public static string ServiceBus_CRM_ConnectionString;
        public static string ServiceBus_CRM_Outgoing_ConnectionString;
        public static string ServiceBus_Pub_Sub_ConnectionString;
        public static string ServiceBus_PubNub_ConnectionString;
        public static string ServiceBus_PubNub_Concierge_ServicePath;
        //public static string ServiceBus_AppActivity_ConnectionString;
        //public static string ServiceBus_AppActivity_ServicePath;

        public static string ServiceBus_Pub_Sub_Outgoing_Account_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_Account_Category_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_CampaignActivity_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_User_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_Beacon_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_Device_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_Proximity_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_SocialNetworkProfile_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_AccountCategoryGroup_ServicePath;
        public static string ServiceBus_Pub_Sub_Outgoing_AccountCategory_ServicePath;

        public static string ServiceBus_Pub_Sub_Account_ServicePath;
        public static string ServiceBus_Pub_Sub_AccountCategory_ServicePath;
        public static string ServiceBus_Pub_Sub_AccountCategoryGroup_ServicePath;
        public static string ServiceBus_Pub_Sub_AccountCategoryMap_ServicePath;
        public static string ServiceBus_Pub_Sub_AccountUsers_ServicePath;
        public static string ServiceBus_Pub_Sub_AddOnOrderLines_ServicePath;
        public static string ServiceBus_Pub_Sub_AppActivity_ServicePath;
        public static string ServiceBus_Pub_Sub_Beacon_ServicePath;
        public static string ServiceBus_Pub_Sub_CampaignActivity_ServicePath;
        public static string ServiceBus_Pub_Sub_CampaignActivityType_ServicePath;
        public static string ServiceBus_Pub_Sub_Conversation_ServicePath;
        public static string ServiceBus_Pub_Sub_Device_ServicePath;
        public static string ServiceBus_Pub_Sub_Engagement_ServicePath;
        public static string ServiceBus_Pub_Sub_iBeaconProximity_ServicePath;
        public static string ServiceBus_Pub_Sub_Interest_ServicePath;
        public static string ServiceBus_Pub_Sub_Location_ServicePath;
        public static string ServiceBus_Pub_Sub_Modifier_ServicePath;
        public static string ServiceBus_Pub_Sub_ModifierGroup_ServicePath;
        public static string ServiceBus_Pub_Sub_OfferKronerDetails_ServicePath;
        public static string ServiceBus_Pub_Sub_Order_ServicePath;
        public static string ServiceBus_Pub_Sub_OrderLines_ServicePath;
        public static string ServiceBus_Pub_Sub_Product_ServicePath;
        public static string ServiceBus_Pub_Sub_ProductCategory_ServicePath;
        public static string ServiceBus_Pub_Sub_ProductVariant_ServicePath;
        public static string ServiceBus_Pub_Sub_Proximity_ServicePath;
        public static string ServiceBus_Pub_Sub_Request_ServicePath;
        public static string ServiceBus_Pub_Sub_Role_ServicePath;
        public static string ServiceBus_Pub_Sub_RolePermission_ServicePath;
        public static string ServiceBus_Pub_Sub_SocialNetworkProfile_ServicePath;
        public static string ServiceBus_Pub_Sub_User_ServicePath;
        public static string ServiceBus_Pub_Sub_UserRoles_ServicePath;
        public static string ServiceBus_Pub_Sub_UserInterests_ServicePath;

        public static string AzureStorageBlob_RootURL;
        public static string AzureStorageBlob_ConnectionString;
        public static string AzureStorageBlob_AccountImageContainer;
        public const int AccountWithServiceCategory = 17;
        public static DateTime SocialFilterDate;

        public static string PubNub_PublishKey;
        public static string PubNub_SubscribeKey;
        public static string PubNub_SecretKey;
        public static bool PubNub_SSlOn;

        public static string NotificationHub_Endpoint;
        public static string NotificationHub_Name;

        public static string JobInterval;

        public static string SendGrid_Email_API_URL;
        public static string SendGrid_Email_API_Key;
        public static string SendGrid_Email_From_Receipts;
        public static string SendGrid_Email_From_WebApp;
        public static string WebAppUrl;
        public static bool EnableLogs;
        public static string BeaconPush;
        #endregion Member Variables

    }
}
