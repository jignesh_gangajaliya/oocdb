﻿
namespace NPRO.Core.Utilities
{
    public class ResourceManager: System.Resources.ResourceManager
    {
        System.Resources.ResourceManager recourceManager;
        public ResourceManager()
        {
            recourceManager = new System.Resources.ResourceManager(typeof(Resources));
        }

        public string GetResourceValue(string resourceName)
        {
            return recourceManager.GetString(resourceName);
        }
    }
}
