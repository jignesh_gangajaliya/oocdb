﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace NPRO.Domain.ESB.PUBSUB.Listener.PUBSUB
{
    public class PubSubManager
    {
        #region Methods

        public void ProcessAccountDBQueueForPubSub(PubSubMessage message)
        {
            var accountService = Bootstrapper.Locate<IAccountService>();
            var accountObj = (Core.Entities.Account)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var account = accountService.Find(accountObj.AccountID);
                    if (account == null)
                    {
                        accountService.AddPubSub(accountObj);
                    }
                    else
                    {
                        accountService.UpdatePubSub(accountObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        accountService.DeletePubSub(accountObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }
        }
        public void ProcessCampaignDBQueueForPubSub(PubSubMessage message)
        {
            var campaignService = Bootstrapper.Locate<ICampaignActivityService>();
            var campaignObj = (Core.Entities.CampaignActivity)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var campaign = campaignService.Find(campaignObj.CampaignActivityID);
                    if (campaign == null)
                    {
                        campaignService.AddPubSub(campaignObj);
                    }
                    else
                    {
                        campaignService.UpdatePubSub(campaignObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        campaignService.DeletePubSub(campaignObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }
        }
        public void ProcessUserInterestDBQueueForPubSub(PubSubMessage message)
        {
            var userInterestService = Bootstrapper.Locate<IUserInterestsService>();
            var userInterestsObj = (UserInterests)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var user = userInterestService.FindByUserInterestId(userInterestsObj.UserInterestsID);
                    if (user == null)
                    {
                        userInterestService.AddPubSub(userInterestsObj);
                    }
                    else
                    {
                        userInterestService.UpdatePubSub(userInterestsObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        userInterestService.DeletePubSub(userInterestsObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessUserDBQueueForPubSub(PubSubMessage message)
        {
            var userService = Bootstrapper.Locate<IUserService>();
            var userObj = (User)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var user = userService.Find(userObj.UserID);
                    if (user == null)
                    {
                        userService.AddPubSub(userObj);
                    }
                    else
                    {
                        userService.UpdatePubSub(userObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        userService.DeletePubSub(userObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessUserRoleDBQueueForPubSub(PubSubMessage message)
        {
            var userRoleService = Bootstrapper.Locate<IUserRolesService>();
            var userRolesObj = (UserRoles)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var user = userRoleService.Find(userRolesObj.RoleUserID).FirstOrDefault();
                    if (user == null)
                        userRoleService.AddPubSub(userRolesObj);
                    else
                        userRoleService.UpdatePubSub(userRolesObj);
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        userRoleService.DeletePubSub(userRolesObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessRoleDBQueueForPubSub(PubSubMessage message)
        {
            var roleService = Bootstrapper.Locate<IRoleService>();
            var rolesObj = (Role)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var role = roleService.Find(rolesObj.RoleID).FirstOrDefault();
                    if (role == null)
                        roleService.AddPubSub(rolesObj);
                    else
                        roleService.UpdatePubSub(rolesObj);
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        roleService.DeletePubSub(rolesObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessBeaconDBQueueForPubSub(PubSubMessage message)
        {
            var beaconService = Bootstrapper.Locate<IBeaconService>();
            var beaconObj = (Beacon)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var beacon = beaconService.Find(beaconObj.BeaconID);
                    if (beacon == null)
                    {
                        beaconService.AddPubSub(beaconObj);
                    }
                    else
                    {
                        beaconService.UpdatePubSub(beaconObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        beaconService.DeletePubSub(beaconObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessLocationDBQueueForPubSub(PubSubMessage message)
        {
            var locationService = Bootstrapper.Locate<ILocationService>();
            var locationObj = (Location)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var beacon = locationService.Find(locationObj.LocationID);
                    if (beacon == null)
                    {
                        locationService.AddPubSub(locationObj);
                    }
                    else
                    {
                        locationService.UpdatePubSub(locationObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        locationService.DeletePubSub(locationObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }
        public void ProcessDeviceDBQueueForPubSub(PubSubMessage message)
        {
            var deviceService = Bootstrapper.Locate<IDeviceService>();
            var deviceObj = (Device)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var device = deviceService.Find(deviceObj.DeviceID).FirstOrDefault();
                    if (device == null)
                    {
                        deviceService.AddPubSub(deviceObj);
                    }
                    else
                    {
                        deviceService.UpdatePubSub(deviceObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        deviceService.DeletePubSub(deviceObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }

        public void ProcessProximityDBQueueForPubSub(PubSubMessage message)
        {
            var proximityService = Bootstrapper.Locate<IProximityService>();
            var proximityObj = (Proximity)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var beacon = proximityService.FindWithiBeaconProximity(proximityObj.ProximityID);
                    if (beacon == null)
                    {
                        proximityService.AddPubSub(proximityObj);
                    }
                    else
                    {
                        proximityService.UpdatePubSub(proximityObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        proximityService.DeletePubSub(proximityObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }

        public void ProcessSocialNetworkDBQueueForPubSub(PubSubMessage message)
        {
            var socialService = Bootstrapper.Locate<ISocialNetworkProfileService>();
            var socialObj = (SocialNetworkProfile)message.Entity;
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var socialNetworkProfile = socialService.Find(socialObj.SocialNetworkProfileID);
                    if (socialNetworkProfile == null)
                    {
                        socialService.AddPubSub(socialObj);
                    }
                    else
                    {
                        socialService.UpdatePubSub(socialObj);
                    }
                    break;
                case "Delete":
                    if (message.Entity != null)
                    {
                        socialService.DeletePubSub(socialObj);
                    }
                    //DeleteCrmAccount(message.CrmID.Value);
                    break;
                default: break;
            }

        }

        public void ProcessAccountCategoryDBQueueForPubSub(PubSubMessage message)
        {
            var accountCategoryService = Bootstrapper.Locate<IAccountCategoryService>();
            var accountCategoryObj = (Core.Entities.AccountCategory)message.Entity;
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var accountCategory = accountCategoryService.Find(accountCategoryObj.AccountCategoryID);
                    if (accountCategory == null)
                    {
                        accountCategoryService.AddPubSub(accountCategoryObj);
                    }
                    else
                    {
                        accountCategoryService.UpdatePubSub(accountCategoryObj);
                    }
                    break;
                case "Delete":
                    accountCategoryService.DeletePubSub(accountCategoryObj);
                    break;
                default: break;
            }
        }
        public void ProcessAccountCategoryGroupDBQueueForPubSub(PubSubMessage message)
        {
            var accountCategoryGroupService = Bootstrapper.Locate<IAccountCategoryGroupService>();
            var accountCategoryGroupObj = (Core.Entities.AccountCategoryGroup)message.Entity;
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var accountCategory = accountCategoryGroupService.Find(accountCategoryGroupObj.AccountCategoryGroupID);
                    if (accountCategory == null)
                    {
                        accountCategoryGroupService.AddPubSub(accountCategoryGroupObj);
                    }
                    else
                    {
                        accountCategoryGroupService.UpdatePubSub(accountCategoryGroupObj);
                    }
                    break;
                case "Delete":
                    accountCategoryGroupService.DeletePubSub(accountCategoryGroupObj);
                    break;
                default: break;
            }
        }

        public void ProcessAccountCategoryMapDBQueueForPubSub(PubSubMessage message)
        {
            var accCatMapObj = (AccountCategoryMap)message.Entity;
            if (accCatMapObj == null)
            {
                return;
            }
            var accCatMapService = Bootstrapper.Locate<IAccountCategoryMapService>();
            switch (message.Operation)
            {

                case "Create":

                case "Update":
                    var accCatMap = accCatMapService.Find(accCatMapObj.AccountCategoryMapID);
                    if (accCatMap == null)
                    {
                        accCatMapService.AddPubSub(accCatMapObj);
                    }
                    else
                    {
                        accCatMapService.UpdatePubSub(accCatMapObj);
                    }
                    break;
                case "Delete":
                    accCatMapService.DeletePubSub(accCatMapObj);
                    break;
                default: break;
            }

        }

        public void ProcessAccountUsersDBQueueForPubSub(PubSubMessage message)
        {
            var accUsersObj = (AccountUsers)message.Entity;
            if (accUsersObj == null)
            {
                return;
            }

            var accUsersService = Bootstrapper.Locate<IAccountUsersService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var accUsers = accUsersService.Find(accUsersObj.AccountUserID).FirstOrDefault();
                    if (accUsers == null)
                    {
                        accUsersService.AddPubSub(accUsersObj);
                    }
                    else
                    {
                        accUsersService.UpdatePubSub(accUsersObj);
                    }
                    break;


                    break;
                case "Delete":
                    accUsersService.DeletePubSub(accUsersObj);
                    break;
                default: break;
            }

        }

        public void ProcessAppActivityQueueForPubSub(PubSubMessage message)
        {
            var appActivityService = Bootstrapper.Locate<IAppActivityService>();
            var appActivityObj = (AppActivity)message.Entity;
            if (appActivityObj == null)
            {
                return;
            }

            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    appActivityService.AddPubSub(appActivityObj);
                    break;
                case "Delete":
                    appActivityService.DeletePubSub(appActivityObj);
                    break;
                default: break;
            }

        }
        public void ProcessAddOnOrderLinesQueueForPubSub(PubSubMessage message)
        {
            var addOnService = Bootstrapper.Locate<IAddOnService>();
            var addOnOrderLinesObj = (AddOnOrderLines)message.Entity;
            if (addOnOrderLinesObj == null)
            {
                return;
            }

            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var addOnOrderLines = addOnService.Find(addOnOrderLinesObj.AddOnID);
                    if (addOnOrderLines == null)
                    {
                        addOnService.AddPubSub(addOnOrderLinesObj);
                    }
                    else
                    {
                        addOnService.UpdatePubSub(addOnOrderLinesObj);
                    }

                    break;
                case "Delete":
                    addOnService.DeletePubSub(addOnOrderLinesObj);
                    break;
                default: break;
            }
        }

        public void ProcessInterestsQueueForPubSub(PubSubMessage message)
        {
            var interestObj = (Interest)message.Entity;
            if (interestObj == null)
            {
                return;
            }

            var interestsService = Bootstrapper.Locate<IInterestService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var interests = interestsService.Find(interestObj.InterestID);
                    if (interests == null)
                    {
                        interestsService.AddPubSub(interestObj);
                    }
                    else
                    {
                        interestsService.UpdatePubSub(interestObj);
                    }
                    break;
                case "Delete":
                    interestsService.DeletePubSub(interestObj);
                    break;
                default: break;
            }

        }

        public void ProcessLocationQueueForPubSub(PubSubMessage message)
        {
            var locationObj = (Location)message.Entity;
            if (locationObj == null)
            {
                return;
            }
            var locationsService = Bootstrapper.Locate<ILocationService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var location = locationsService.Find(locationObj.LocationID);
                    if (location == null)
                    {
                        locationsService.AddPubSub(locationObj);
                    }
                    else
                    {
                        locationsService.UpdatePubSub(locationObj);
                    }
                    break;
                case "Delete":
                    locationsService.DeletePubSub(locationObj);
                    break;
                default: break;
            }

        }
        public void ProcessCampaignActivityTypeQueueForPubSub(PubSubMessage message)
        {
            var campaignActivityTypeObj = (CampaignActivityType)message.Entity;
            if (campaignActivityTypeObj == null)
            {
                return;
            }

            var campaignActivityTypeService = Bootstrapper.Locate<ICampaignActivityTypeService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var campaignActivityType = campaignActivityTypeService.Find(campaignActivityTypeObj.CampaignActivityTypeID);
                    if (campaignActivityType == null)
                    {
                        campaignActivityTypeService.AddPubSub(campaignActivityTypeObj);
                    }
                    else
                    {
                        campaignActivityTypeService.UpdatePubSub(campaignActivityTypeObj);
                    }
                    break;
                case "Delete":
                    campaignActivityTypeService.DeletePubSub(campaignActivityTypeObj);
                    break;
                default: break;
            }
        }
        public void ProcessConversationTopicQueueForPubSub(PubSubMessage message)
        {
            var conversationObj = (Core.Concierge.Entities.Conversation)message.Entity;
            if (conversationObj == null)
            {
                return;
            }

            var conversationService = Bootstrapper.Locate<IConversationService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var conversation = conversationService.Find(conversationObj.ConversationID);
                    if (conversation == null)
                    {
                        conversationService.AddPubSub(conversationObj);
                    }
                    else
                    {
                        conversationService.UpdatePubSub(conversationObj);
                    }
                    break;
                case "Delete":
                    conversationService.DeletePubSub(conversationObj);
                    break;
                default: break;
            }
        }
        public void ProcessEngagementTopicQueueForPubSub(PubSubMessage message)
        {
            var engagementObj = (Engagement)message.Entity;
            if (engagementObj == null)
            {
                return;
            }

            var engagementService = Bootstrapper.Locate<IEngagementService>();
            switch (message.Operation)
            {
                case "Create":

                case "Update":
                    var engagement = engagementService.Find(engagementObj.EngagementID);
                    if (engagement == null)
                    {
                        engagementService.AddPubSub(engagementObj);
                    }
                    else
                    {
                        engagementService.UpdatePubSub(engagementObj);
                    }
                    break;
                case "Delete":
                    engagementService.DeletePubSub(engagementObj);
                    break;
                default: break;
            }
        }
        public void ProcessBeaconProximityTopicQueueForPubSub(PubSubMessage message)
        {

            var iBeaconProximityObj = (iBeaconProximity)message.Entity;
            if (iBeaconProximityObj == null)
            {
                return;
            }

            var iBeaconProximityService = Bootstrapper.Locate<IiBeaconProximityService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var iBeacon = iBeaconProximityService.Find(iBeaconProximityObj.iBeaconProximityID);
                    if (iBeacon == null)
                    {
                        iBeaconProximityService.AddPubSub(iBeaconProximityObj);
                    }
                    else
                    {
                        iBeaconProximityService.UpdatePubSub(iBeaconProximityObj);
                    }
                    break;
                case "Delete":
                    iBeaconProximityService.DeletePubSub(iBeaconProximityObj);
                    break;
                default: break;
            }


        }
        public void ProcessModifierTopicQueueForPubSub(PubSubMessage message)
        {
            var modifierObj = (Modifier)message.Entity;
            if (modifierObj == null)
            {
                return;
            }
            var modifierService = Bootstrapper.Locate<IModifierService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var modifier = modifierService.Find(modifierObj.ModifierID);
                    if (modifier == null)
                    {
                        modifierService.AddPubSub(modifierObj);
                    }
                    else
                    {
                        modifierService.UpdatePubSub(modifierObj);
                    }
                    break;
                case "Delete":
                    modifierService.DeletePubSub(modifierObj);
                    break;
                default: break;
            }
        }

        public void ProcessModifierGroupTopicQueueForPubSub(PubSubMessage message)
        {
            var modifierGroupObj = (ModifierGroup)message.Entity;
            if (modifierGroupObj == null)
            {
                return;
            }
            var modifierGroupService = Bootstrapper.Locate<IModifierGroupService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var modifierGroup = modifierGroupService.Find(modifierGroupObj.ModifierGroupID);
                    if (modifierGroup == null)
                    {
                        modifierGroupService.AddPubSub(modifierGroupObj);
                    }
                    else
                    {
                        modifierGroupService.UpdatePubSub(modifierGroupObj);
                    }
                    break;
                case "Delete":
                    modifierGroupService.DeletePubSub(modifierGroupObj);
                    break;
                default: break;
            }
        }

        public void ProcessOfferKronerDetailsTopicQueueForPubSub(PubSubMessage message)
        {
            var offerKronerDetailsObj = (OfferKronerDetails)message.Entity;
            if (offerKronerDetailsObj == null)
            {
                return;
            }

            var OfferKronerDetailsService = Bootstrapper.Locate<IOfferKronerDetailsService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var offer = OfferKronerDetailsService.Find(offerKronerDetailsObj.OfferRedeemedID);
                    if (offer == null)
                    {
                        OfferKronerDetailsService.AddPubSub(offerKronerDetailsObj);
                    }
                    else
                    {
                        OfferKronerDetailsService.UpdatePubSub(offerKronerDetailsObj);
                    }
                    break;
                case "Delete":
                    OfferKronerDetailsService.DeletePubSub(offerKronerDetailsObj);
                    break;
                default: break;
            }
        }
        public void ProcessOrderTopicQueueForPubSub(PubSubMessage message)
        {
            var orderObj = (Order)message.Entity;
            if (orderObj == null)
            {
                return;
            }
            var orderService = Bootstrapper.Locate<IOrderService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var order = orderService.Find(orderObj.OrderID);
                    if (order == null)
                    {
                        orderService.AddPubSub(orderObj);
                    }
                    else
                    {
                        orderService.UpdatePubSub(orderObj);
                    }
                    break;
                case "Delete":
                    orderService.DeletePubSub(orderObj);
                    break;
                default: break;
            }
        }

        public void ProcessOrderLinesTopicQueueForPubSub(PubSubMessage message)
        {
            var orderLinesObj = (OrderLines)message.Entity;
            if (orderLinesObj == null)
            {
                return;
            }

            var orderLinesService = Bootstrapper.Locate<IOrderLinesService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var orderLine = orderLinesService.Find(orderLinesObj.OrderLineID);
                    if (orderLine == null)
                    {
                        orderLinesService.AddPubSub(orderLinesObj);
                    }
                    else
                    {
                        orderLinesService.UpdatePubSub(orderLinesObj);
                    }
                    break;
                case "Delete":
                    orderLinesService.DeletePubSub(orderLinesObj);
                    break;
                default: break;
            }
        }

        public void ProcessProductTopicQueueForPubSub(PubSubMessage message)
        {
            var productObj = (Core.FastTrackOrder.Entities.Product)message.Entity;
            if (productObj == null)
            {
                return;
            }
            var productService = Bootstrapper.Locate<IProductService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var product = productService.Find(productObj.ProductID);
                    if (product == null)
                    {
                        productService.AddPubSub(productObj);
                    }
                    else
                    {
                        productService.UpdatePubSub(productObj);
                    }
                    break;
                case "Delete":
                    productService.DeletePubSub(productObj);
                    break;
                default: break;
            }
        }
        public void ProcessProductCategoryTopicQueueForPubSub(PubSubMessage message)
        {

            var productCategoryObj = (Core.FastTrackOrder.Entities.ProductCategory)message.Entity;
            if (productCategoryObj == null)
            {
                return;
            }

            var productCategoryService = Bootstrapper.Locate<IProductCategoryService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var productCategory = productCategoryService.Find(productCategoryObj.ProductCategoryID);
                    if (productCategory == null)
                    {
                        productCategoryService.AddPubSub(productCategoryObj);
                    }
                    else
                    {
                        productCategoryService.UpdatePubSub(productCategoryObj);
                    }
                    break;
                case "Delete":
                    productCategoryService.DeletePubSub(productCategoryObj);
                    break;
                default: break;
            }
        }
        public void ProcessProductVariantTopicQueueForPubSub(PubSubMessage message)
        {

            var productVariantObj = (Core.FastTrackOrder.Entities.ProductVariant)message.Entity;
            if (productVariantObj == null)
            {
                return;
            }
            var productVariantService = Bootstrapper.Locate<IProductVariantService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var productVarient = productVariantService.Find(productVariantObj.VariantID);
                    if (productVarient == null)
                    {
                        productVariantService.AddPubSub(productVariantObj);
                    }
                    else
                    {
                        productVariantService.UpdatePubSub(productVariantObj);
                    }
                    break;
                case "Delete":
                    productVariantService.DeletePubSub(productVariantObj);
                    break;
                default: break;
            }
        }

        public void ProcessRequestTopicQueueForPubSub(PubSubMessage message)
        {

            var requestObj = (Request)message.Entity;
            if (requestObj == null)
            {
                return;
            }

            var requestService = Bootstrapper.Locate<IRequestService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":
                    var request = requestService.Find(requestObj.RequestID);
                    if (request == null)
                    {
                        requestService.AddPubSub(requestObj);
                    }
                    else
                    {
                        requestService.UpdatePubSub(requestObj);
                    }
                    break;
                case "Delete":
                    requestService.DeletePubSub(requestObj);
                    break;
                default: break;
            }
        }
        public void ProcessRolePermissionTopicQueueForPubSub(PubSubMessage message)
        {
            var rolePermissionObj = (RolePermission)message.Entity;
            if (rolePermissionObj == null)
            {
                return;
            }

            var rolePermissionService = Bootstrapper.Locate<IRolePermissionService>();
            switch (message.Operation)
            {
                case "Create":
                case "Update":

                    var rolePermission = rolePermissionService.Find(rolePermissionObj.PermissionID);
                    if (rolePermission == null)
                    {
                        rolePermissionService.AddPubSub(rolePermissionObj);
                    }
                    else
                    {
                        rolePermissionService.UpdatePubSub(rolePermissionObj);
                    }
                    break;
                case "Delete":
                    rolePermissionService.DeletePubSub(rolePermissionObj);
                    break;
                default: break;
            }
        }


        #endregion Methods
    }
}
