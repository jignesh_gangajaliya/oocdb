﻿using System;
using Microsoft.ServiceBus.Messaging;
using NPRO.Domain.CRMESBListener.WorkerRole.ServiceBus;
using System.Threading.Tasks;
using System.Threading;

namespace NPRO.Cloud.ServiceBus.Topics
{
    public class TopicSubscribeClientWrapper 
    {
        #region Member Variables

        private readonly string _connectionString;
        public MessageSession _session;
        private SubscriptionClient _subscriptionClient;
        //public static Func<PubSubMessage, Task> messageCallback;
        //private readonly string _connectionString;
        private readonly string _topicName;
        public SubscriptionClient storedSubscription { get { return _subscriptionClient; } }
        #endregion Member Variables

        #region Properties

        public string SubscriptionName { get; set; }

        #endregion Properties

        #region Constructors

        public TopicSubscribeClientWrapper(string connectionString, string topicName, string subscriptionName)
        {
            SubscriptionName = subscriptionName;
            _connectionString = connectionString;
            _topicName = topicName;
            if (_subscriptionClient == null)
            {
                _subscriptionClient = SubscriptionClient.CreateFromConnectionString(_connectionString, _topicName, SubscriptionName);
                _subscriptionClient.RegisterSessionHandlerAsync(typeof(MessageReader), new SessionHandlerOptions { AutoComplete = false });
            }
        }
        public void OnMessageAsync(Func<BrokeredMessage, Task> onMessageCallback, OnMessageOptions onMessageOptions)
        {

            // _subscriptionClient = SubscriptionClient.CreateFromConnectionString(_connectionString, _topicName, SubscriptionName);
            //if (_subscriptionClient == null)
            //{
            //    _subscriptionClient = SubscriptionClient.CreateFromConnectionString(_connectionString, _topicName, SubscriptionName);
            //    _subscriptionClient.RegisterSessionHandlerAsync(typeof(MessageReader), new SessionHandlerOptions { AutoComplete = false });
            //}
        }

        #endregion Constructors

        #region Event Handlers

        public void OnMessageReceipt()
        {

        }
        #endregion Event Handlers

        #region Methods

        public void Close()
        {
            if (_subscriptionClient != null)
            {
                _subscriptionClient.Close();
            }

        }



        #endregion Methods
    }

}
