﻿using Microsoft.ServiceBus.Messaging;
using NPRO.Core.Entities.Models;
using NPRO.Domain.ESB.PUBSUB.Listener.PUBSUB;
using NPRO.Domain.Integrations.MSCRM;
using System;
using System.Collections.Generic;
using System.IO;


namespace NPRO.Domain.CRMESBListener.WorkerRole.ServiceBus
{
    public class MessageReader : IMessageSessionHandler
    {
        public void OnCloseSession(MessageSession session)
        {
            System.Diagnostics.Trace.WriteLine("OnCloseSession");
        }

        public void OnMessage(MessageSession session, BrokeredMessage message)
        {
            try
            {
                // MessageSession session = _subscriptionClient.AcceptMessageSession(TimeSpan.FromSeconds(2));
                if (message != null)
                {
                    MemoryStream largeMessageStream = new MemoryStream();
                    Dictionary<int, Stream> orderedDic = new Dictionary<int, Stream>();
                    Stream subMessageStream = message.GetBody<Stream>();
                    orderedDic.Add(int.Parse(message.Label), subMessageStream);
                    message.Complete();
                    while (true)
                    {
                        BrokeredMessage subMessage = session.Receive(TimeSpan.FromSeconds(5));

                        if (subMessage != null)
                        {
                            Stream subMessageStr = subMessage.GetBody<Stream>();
                            orderedDic.Add(int.Parse(subMessage.Label), subMessageStr);
                            subMessage.Complete();
                            //Console.Write(".");
                        }
                        else
                        {
                            //Console.WriteLine("Done!");

                            for (int i = 1; i <= orderedDic.Count; i++)
                            {
                                Stream MessageStream;
                                orderedDic.TryGetValue(i, out MessageStream);
                                if (MessageStream != null)
                                    MessageStream.CopyTo(largeMessageStream);
                            }
                            break;
                        }
                    }

                    BrokeredMessage largeMessage = new BrokeredMessage(largeMessageStream, true);
                    var messages = largeMessage.GetBody<Core.Entities.Models.PubSubMessage>();
                    ProcessMessage(messages);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("OnMessage: " + ex.ToString());
            }

        }
        public void OnSessionLost(Exception exception)
        {
            System.Diagnostics.Trace.WriteLine("OnSessionLost");
        }
        private async System.Threading.Tasks.Task ProcessMessage(PubSubMessage message)
        {
            try
            {
                switch (message.Name.ToLower())
                {
                    case "account":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAccountDBQueueForPubSub(message));
                        break;
                    case "accountcategory":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAccountCategoryDBQueueForPubSub(message));
                        break;
                    case "campaignactivity":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessCampaignDBQueueForPubSub(message));
                        break;
                    case "user":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessUserDBQueueForPubSub(message));
                        break;
                    case "location":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessLocationDBQueueForPubSub(message));
                        break;
                    case "beacon":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessBeaconDBQueueForPubSub(message));
                        break;
                    case "device":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessDeviceDBQueueForPubSub(message));
                        break;
                    case "proximity":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessProximityDBQueueForPubSub(message));
                        break;
                    case "socialnetworkprofile":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessSocialNetworkDBQueueForPubSub(message));
                        break;
                    case "accountcategorymap":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAccountCategoryMapDBQueueForPubSub(message));
                        break;
                    case "accountusers":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAccountUsersDBQueueForPubSub(message));
                        break;
                    case "accountcategorygroup":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAccountCategoryGroupDBQueueForPubSub(message));
                        break;
                    case "interest":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessInterestsQueueForPubSub(message));
                        break;
                    case "addonorderlines":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAddOnOrderLinesQueueForPubSub(message));
                        break;
                    case "appactivity":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessAppActivityQueueForPubSub(message));
                        break;
                    case "conversation":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessConversationTopicQueueForPubSub(message));
                        break;
                    case "engagement":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessEngagementTopicQueueForPubSub(message));
                        break;
                    case "ibeaconproximity":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessBeaconProximityTopicQueueForPubSub(message));
                        break;
                    case "modifier":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessModifierTopicQueueForPubSub(message));
                        break;
                    case "modifiergroup":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessModifierGroupTopicQueueForPubSub(message));
                        break;
                    case "offerkronerdetails":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessOfferKronerDetailsTopicQueueForPubSub(message));
                        break;
                    case "orderlines":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessOrderLinesTopicQueueForPubSub(message));
                        break;
                    case "order":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessOrderTopicQueueForPubSub(message));
                        break;
                    case "product":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessProductTopicQueueForPubSub(message));
                        break;
                    case "productcategory":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessProductCategoryTopicQueueForPubSub(message));
                        break;
                    case "productvariant":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessProductVariantTopicQueueForPubSub(message));
                        break;
                    case "request":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessRequestTopicQueueForPubSub(message));
                        break;
                    case "role":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessRoleDBQueueForPubSub(message));
                        break;
                    case "rolepermission":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessRolePermissionTopicQueueForPubSub(message));
                        break;
                    case "userroles":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessUserRoleDBQueueForPubSub(message));
                        break;
                    case "userinterests":
                        await System.Threading.Tasks.Task.Run(() => new PubSubManager().ProcessUserInterestDBQueueForPubSub(message));
                        break;

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("ProcessMessage: " + ex.ToString());
            }

        }
    }
}
