﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Threading;
using Microsoft.Azure.NotificationHubs;

namespace NPRO.Domain.Integrations.ServiceBus
{
    public class PubSubMessageListener
    {
        #region Member Variables

        

        #endregion Member Variables

        #region Constructors

        private PubSubMessageListener()
        {
            
        }

        #endregion Constructors

        #region Methods
        private void OnExceptionReceived(object sender, ExceptionReceivedEventArgs e)
        {
            if (e != null && e.Exception != null)
            {

            }
        }
        ////commented for earlier appraoch
        //private async System.Threading.Tasks.Task OnMessageArrived(BrokeredMessage message)
        //{
        //    PubSubMessage completeMsg;
        //    if (message != null)
        //    {
        //        LargeMessageManager largeMessageReader = new LargeMessageManager();
        //        //completeMsg = largeMessageReader.Receive(message);
        //        try
        //        {
        //           // PubSubMessage processMessage = completeMsg.GetBody<PubSubMessage>();
        //            switch (message.ContentType.ToLower())
        //            {
        //                case "account":
        //                    largeMessageReader.storeSubscription =  accountTopicClient.storedSubscription;
        //                    largeMessageReader._session = accountTopicClient._session;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "campaignactivity":
        //                    largeMessageReader.storeSubscription = campaignActivityTopicClient.storedSubscription;
        //                    largeMessageReader._session = campaignActivityTopicClient._session;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new CampaignActivityManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "user":
        //                    largeMessageReader.storeSubscription = userTopicClient.storedSubscription;
        //                    largeMessageReader._session = userTopicClient._session;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new UserManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "beacon":
        //                    largeMessageReader.storeSubscription = beaconTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new BeaconManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "device":
        //                    largeMessageReader.storeSubscription = deviceTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new DeviceManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "proximity":
        //                    largeMessageReader.storeSubscription = proximityTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new ProximityManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "socialnetworkprofile":
        //                    largeMessageReader.storeSubscription = socialNetworkProfileTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new SocialNetworkProfileManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "accountcategory":
        //                    largeMessageReader.storeSubscription = accountCategoryTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountCategoryManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "accountcategorymap":
        //                    largeMessageReader.storeSubscription = accountCategoryMapTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountCategoryMapManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "accountusers":
        //                    largeMessageReader.storeSubscription = accountUsersTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    //System.Threading.Tasks.Task.Run(() => new AccountUsersManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "addonorderlines":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "accountcategorygroup":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "appactivity":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "interest":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "location":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "campaignactivitytype":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "conversation":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "engagement":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "ibeaconproximity":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "modifier":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "modifiergroup":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "offerkronerdetails":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "order":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "orderlines":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "product":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "productcategory":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "productvariant":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "request":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "role":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "rolepermission":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "userroles":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;
        //                case "userinterests":
        //                    largeMessageReader.storeSubscription = accountTopicClient.storedSubscription;
        //                    completeMsg = largeMessageReader.Receive(message);
        //                    System.Threading.Tasks.Task.Run(() => new AccountManager().ProcessDBQueueForPubSub(completeMsg));
        //                    break;

            //            }
            //            //await ProcessDBMessage(message.GetBody<ServiceBusMessage>());


            //        }
            //        catch (Exception ex)
            //        {
            //            System.Diagnostics.Trace.WriteLine("OnMessageArrived: " + ex.Message.ToString());
            //        }
            //    }

            //}

        public static PubSubMessageListener GetDBMessageListner()
        {
            
            return null;
        }
        //commented for earlier approach reference
        public void Listen()
        {
        

        }

        #endregion Methods
    }
}
